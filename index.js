const os = require('os');
const parse = require('csv-parse/lib/sync');
const fs = require('fs-extra');
const chalk = require('chalk');
const services = require('./services.js');
const utils = require('./utils.js');
const { exit } = require('process');

let env;

const readCSV = async (file) => {
  const content = await fs.readFile(file);
  // parse and remove first row (header)
  const records = parse(content);
  records.shift();
  let docsInfo = records.map((record) => {
    return {
      processId: record[1],
      crm: record[2] != 'NA' && record[2] != '' ? record[2] : null,
      language: record[6],
      tridionPath: utils.fixURL(record[5]),
      aemPath: utils.fixURL(record[7]),
    };
  });
  // skip invalid entries
  docsInfo = docsInfo.filter((docInfo) => {
    const pidRegex = /[A-Z]{2}[0-9]{6}/gm;
    const valid =
      docInfo.processId &&
      pidRegex.test(docInfo.processId) &&
      docInfo.language &&
      docInfo.language.length === 2 &&
      docInfo.tridionPath &&
      utils.isValidUrl(docInfo.tridionPath) &&
      docInfo.aemPath &&
      utils.isValidUrl(docInfo.aemPath);
    if (!valid) {
      console.info(`skipping ${JSON.stringify(docInfo)}`);
    }
    return valid;
  });

  return docsInfo;
};

const checkDocument = async (docInfo, tsec, index) => {
  console.info('━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━');
  console.info(
    `${chalk.blue(
      `1. Checking Document ${docInfo.processId} ${
        docInfo.crm ? docInfo.crm : ''
      }`
    )} ${docInfo.language}`
  );
  console.info(`${chalk.blue(`2. URL Tridion:`)} ${docInfo.tridionPath}`);
  console.info(`${chalk.blue(`3. URL AEM:`)} ${docInfo.aemPath}`);

  //1. create output folder
  const folder = createOutputFolder(
    './_OUT',
    index,
    docInfo.processId,
    docInfo.crm,
    docInfo.language
  );
  console.info(`${chalk.green(`4. Output dir:`)} ${folder}`);

  //2. get traceability info
  let tracInfo = null;
  try {
    tracInfo = await services.getTraceabilityInfo(
      docInfo.processId,
      docInfo.crm,
      docInfo.language,
      tsec,
      env
    );
    console.info(
      `${chalk.green(`5. traceabilityInfo:`)} ${JSON.stringify(
        tracInfo,
        null,
        2
      )}`
    );
  } catch (e) {
    console.error(
      `${chalk.red(`5. ERROR getting traceability info 🛑: `)} ${JSON.stringify(
        e,
        null,
        2
      )}`
    );
    return;
  }

  //3. compare aem url returned by traceability and declared in CSV
  const pdfInTracInfo = utils.fixURL(tracInfo.documents[0].link.href);
  if (!utils.isAEM(pdfInTracInfo)) {
    console.error(
      `${chalk.red(
        `6. PDF returned by traceability is NOT AEM 🛑 :`
      )}${pdfInTracInfo}`
    );
    return;
  }
  console.info(
    `${chalk.blue(`6. Comparing AEM URL returned by traceability service`)}`
  );
  if (docInfo.aemPath !== pdfInTracInfo) {
    console.error(
      `${chalk.red(`7. Different URL in traceability and CSV 🛑`)}`
    );
    console.error(`${chalk.red(`7. Traceability:`)}${pdfInTracInfo}`);
    console.error(`${chalk.red(`7. CSV:`)}${docInfo.aemPath}`);
    return;
  } else {
    console.info(
      `${chalk.green(`7. Returned expected AEM PDF URL ${docInfo.aemPath}`)}`
    );
  }

  //4. Verify the URL returned by traceability service matches the environment
  // if (!utils.validDomain(pdfInTracInfo, env)) {
  //   console.error(`${chalk.red(`7. Invalid domain for environment ${env}: `)}${pdfInTracInfo}`);
  //   return;
  // }

  //5. download document s
  
  let tridionFile;
  let aemFile;
  try {
    console.info(`${chalk.green(`8. Downloading `)}${docInfo.tridionPath}`);
    tridionFile = await services.downloadFile(
      docInfo.tridionPath,
      folder,
      'tridion.pdf'
    );
    //6. download aem document
    console.info(`${chalk.green(`9. Downloading `)}${pdfInTracInfo}`);
    aemFile = await services.downloadFile(pdfInTracInfo, folder, 'aem.pdf');
  } catch (e) {
    console.error('9. Error dowloading file 🛑 '+e);
    return;
  }

  //7. diff tridion vs aem
  const diffStatus = utils.comparePdfs(tridionFile, aemFile, folder);
  diffStatus
    ? console.error('10. PDFs are different!!! 🛑')
    : console.info('10. PDFs are identical ✅ 🍺');
};

const createOutputFolder = (base, index, processId, crm, language) => {
  const dir = crm
    ? `${base}/${String(index).padStart(
        3,
        '0'
      )}_${processId}_${crm}_${language.toUpperCase()}`
    : `${base}/${String(index).padStart(
        3,
        '0'
      )}_${processId}_${language.toUpperCase()}`;
  fs.ensureDirSync(dir);
  fs.emptyDir(dir);
  return dir;
};

(async function () {
  utils.setupLogs();

  fs.removeSync('./_OUT');

  var args = process.argv.slice(2);
  env = args[0];
  if (!env || !(env === 'ei' || env === 'pr')) {
    console.info('Missing environment param');
    process.exit(1);
  }

  let file = args[1];
  if (!file) {
    file = './input.csv';
    console.warn(
      `No 'file' param detected. Using 'input.csv' as default value`
    );
  }
  console.info(`Input file: ${file}`);

  const userCredentials = await utils.getUserCredentials();
  // const userCredentials = {
  //   username: '0019-012345619R',
  //   password: '123456',
  // };
  // console.info(userCredentials);

  const tsec = await services.getTsec(userCredentials, env);
  if (!tsec) {
    process.exit(1);
  }
  console.info(`${chalk.blue('tsec:')} ${tsec}`);

  const docs = await readCSV(file);

  // use 'for' instead of 'forEach' for sync reasons
  for (let i = 0; i < docs.length; i++) {
    await checkDocument(docs[i], tsec, i);
  }
})();
