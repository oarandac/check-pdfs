const fetch = require('node-fetch');
const utils = require('./utils.js');
//const download = require('download-file');
const wget = require('node-wget-js');
const chalk = require('chalk');

//const log = console.log;
// const log = require('simple-node-logger').createSimpleLogger(
//   './_OUT/project.log'
// );

const getTraceabilityInfo = async (processId, crm, language, tsec, env) => {
  const endpoint = crm
    ? utils.fixDomain(
        `https://ei.bbva.es/ASO/gdpr/v0/processes/${processId}/documents?language=${language}&crm=${crm}`,
        env
      )
    : utils.fixDomain(
        `https://ei.bbva.es/ASO/gdpr/v0/processes/${processId}/documents?language=${language}`,
        env
      );
  const response = await fetch(endpoint, {
    headers: { 'Content-Type': 'application/json', tsec },
  });
  const resBody = await response.json();
  if (!response.ok) {
    return Promise.reject({ status: response.status, body: resBody });
  }
  return Promise.resolve(resBody);
};

const getTsec = async (userCredentials, env) => {
  console.info('Getting tsec...');
  const body = {
    authentication: {
      consumerID: '00000001',
      authenticationType: '02',
      userID: userCredentials.username,
      authenticationData: [
        {
          authenticationData: [userCredentials.password],
          idAuthenticationData: 'password',
        },
      ],
    },
  };

  const endpoint = utils.fixDomain(
    'https://ei.bbva.es/ASO/TechArchitecture/grantingTickets/V02',
    env
  );

  const res = await fetch(endpoint, {
    method: 'post',
    body: JSON.stringify(body),
    headers: { 'Content-Type': 'application/json' },
  });

  if (!res.ok) {
    console.info(`Error getting tsec.`);
    return null;
  }
  tsec = res && res.headers.get('tsec');
  return tsec;
};

const downloadFile = async (url, folder, name) => {
  const dest = `${folder}/${name}`;
  return new Promise((resolve, reject) => {
    wget(
      {
        url,
        dest
      },
      function (error, response, body) {
        if (error) {
          console.info(`${chalk.red(`ERROR downloading ${url}:`)} ${error}`);
          reject(null);
        } else {
          resolve(dest);
        }
      }
    );
  });
};

module.exports = {
  getTsec,
  getTraceabilityInfo,
  downloadFile,
};
