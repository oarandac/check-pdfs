const fs = require('fs-extra');
const prompt = require('prompt');
const spawn = require('cross-spawn');
const stripAnsi = require('strip-ansi');

const DOMAIN_BY_ENV = {
  ei: 'ei.bbva.es',
  pr: 'www.bbva.es',
};

const fixDomain = (u, env) => {
  const url = new URL(u);
  url.hostname = DOMAIN_BY_ENV[env];
  return url.toString();
};

const validDomain = (url, env) => {
  const u = new URL(url);
  return u.hostname === DOMAIN_BY_ENV[env];
};

const isValidUrl = (s) => {
  try {
    new URL(s);
  } catch (_) {
    return false;
  }

  return true;
};

const getUserCredentials = async () => {
  var schema = {
    properties: {
      username: {
        pattern: /^[a-zA-Z0-9]+$/,
        message: 'Username must be only letters',
        required: true,
      },
      password: {
        description: 'Password',
        message: 'You must enter a valid password',
        hidden: true,
        required: true,
      },
    },
  };
  prompt.message = '';
  prompt.start();

  return new Promise((resolve, reject) => {
    prompt.get(schema, function (err, result) {
      const creds = {
        username: `0019-${result.username.toUpperCase().padStart(10, '0')}`,
        password: result.password,
      };
      resolve(creds);
    });
  });
};

const comparePdfs = (pdf1, pdf2, folder) => {
  const result = spawn.sync(
    'diff-pdf',
    [`--output-diff=${folder}/diff.pdf`, pdf1, pdf2],
    { stdio: 'inherit' }
  );
  return result.status;
};

const isAEM = (u) => {
  const aemPattern = '/content/dam/public-web/bbvaes/';
  const url = new URL(u);
  return url.pathname.startsWith(aemPattern);
};

const fixURL = (u) => {
  const replaceAll = (originalString, find, replace) => {
    return originalString.replace(new RegExp(find, 'g'), replace);
  };

  const url = new URL(u);
  url.pathname = replaceAll(url.pathname,"//","/");
  return url.toString();
};

const setupLogs = () => {
  const logFile = './log.log';
  try {
    fs.removeSync(logFile);
  } catch (err) {
    console.error('Cannot delete log file ' + err);
  }
  let outputFileStream = fs.createWriteStream(logFile, { flags: 'a' });

  const originalWriteOut = process.stdout.write;
  const originalWriteErr = process.stderr.write;

  process.stdout.write = function () {
    originalWriteOut.apply(process.stdout, arguments);
    outputFileStream.write.apply(outputFileStream, [stripAnsi(arguments[0])]);
  };
  process.stderr.write = function () {
    originalWriteErr.apply(process.stderr, arguments);
    outputFileStream.write.apply(outputFileStream, [stripAnsi(arguments[0])]);
  };

  process.on('uncaughtException', function (err) {
    console.error(err && err.stack ? err.stack : err);
  });
};

module.exports = {
  fixDomain,
  getUserCredentials,
  isValidUrl,
  comparePdfs,
  setupLogs,
  validDomain,
  isAEM,
  fixURL,
};
